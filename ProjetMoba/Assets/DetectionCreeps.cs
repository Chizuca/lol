﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionCreeps : MonoBehaviour
{
    // GameObject qui sera détecté dans le SphereCast
    public GameObject currentHitGameObject;
    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;
    private Vector3 originTower;
    private Vector3 directionTower;

    private float currentHitDistance;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        originTower = transform.position;
        directionTower = transform.forward;
        RaycastHit hit;
        
        // Détection d'éléments autour de la tour
        if (Physics.SphereCast(originTower, sphereRadius, directionTower, out hit, maxDistance, layerMask,
            QueryTriggerInteraction.UseGlobal))
        {
            Debug.Log("Quelque chose a été détecté");
            Debug.DrawRay(originTower, directionTower, Color.red);
            currentHitGameObject = hit.transform.gameObject;
            currentHitDistance = hit.distance;
        }
        else
        {
            currentHitGameObject = null;
            currentHitDistance = maxDistance;
        }
    }
}
