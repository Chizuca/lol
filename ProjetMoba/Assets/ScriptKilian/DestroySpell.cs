﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySpell : MonoBehaviour
{
    public float cooldown = 1f;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this, 1f);
    }

    void Update()
    {
        cooldown -= Time.deltaTime;
        if (cooldown <= 0)
        {
            Capacite1 capacite1 = FindObjectOfType<Capacite1>();
            capacite1._isCasting = false;
        }
    }
}
