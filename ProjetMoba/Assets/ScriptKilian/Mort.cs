﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mort : MonoBehaviour
{
    public Player player;

    public float timerBeforeRespawn, timerQuiSecoule;
    public GameObject deathText;
    
    // Start is called before the first frame update
    void Start()
    {
        ActualisationTimer();
    }

    // Update is called once per frame
    public void Update()
    {
        if (player.stats.pv <= 0)
        {
            player.isDead = true;
            deathText.SetActive(true);
            timerQuiSecoule -= Time.deltaTime;
            
            if (timerQuiSecoule <= 0 && player.isDead)
            {
                //transform.position = spawn.transform.position;
                deathText.SetActive(false);
                timerBeforeRespawn += 3;
                player.stats.pv = player.stats.pvMax;
                player.stats.mana = player.stats.manaMax;
                ActualisationTimer();
                player.isDead = false;
            }
        }
    }

    void ActualisationTimer()
    {
        timerQuiSecoule = timerBeforeRespawn;
    }
}
