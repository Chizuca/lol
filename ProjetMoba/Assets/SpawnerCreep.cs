﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerCreep : MonoBehaviour
{
    public GameObject creepPrefabs;
    public int nbSpawn;
    public float timerSpawn = 0f;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (timerSpawn <= 0f)
        {
            InvokeRepeating("SpawnVert", 0f, 0.5f);
        }
        else
        {
            timerSpawn -= Time.deltaTime;
        }
        
        if (nbSpawn >= 4)
        {
            CancelInvoke("SpawnVert");
            if (timerSpawn <= 0f)
            {
                timerSpawn = 10f;
                nbSpawn = 0;
            }
            
        }
    }
    
    public void SpawnVert()
    {
        Instantiate(creepPrefabs, transform.position,Quaternion.identity);
        nbSpawn += 1;
    }
}
