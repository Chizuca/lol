﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Playermore : MonoBehaviour
{
    public int hpTest, manaTest, moneyTest , manaMaxTest, hpMaxTest;
    public TextMeshProUGUI hpText,manaText,moneyText;
    public GameObject shopUI ,item1 ,item2 , item3 , item4;
    void Start()
    {
        
    }

    void Update()
    {
        hpText.text = hpTest.ToString() + "/" + hpMaxTest.ToString();
        manaText.text = manaTest.ToString() +"/" + manaMaxTest.ToString();
        moneyText.text = moneyTest.ToString() + "$";
    }

    public void Shop()
    {
        shopUI.SetActive(true);
    }

    public void Achat(int valeur , GameObject item)
    {
        if (moneyTest >= valeur)
        {
            item = item1;
        }
    }
}
