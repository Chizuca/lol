﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Creep : MonoBehaviour
{
    public int life;
    public int damage;
    
    private NavMeshAgent _agent;
    private Transform _moveTarget;
    public Transform finishDirection;
    
    // Start is called before the first frame update
    void Start()
    {
        _agent = GetComponent(typeof(NavMeshAgent)) as NavMeshAgent;
    }

    // Update is called once per frame
    void Update()
    {
        if (life <= 0)
        {
            // Detruire le creep
            // Donne + de gold
        }
        
        _agent.SetDestination(finishDirection.position);
    }   
}
